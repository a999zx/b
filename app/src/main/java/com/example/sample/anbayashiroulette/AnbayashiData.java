package com.example.sample.anbayashiroulette;

/**
 * Created by SatoshiTsuchiya on 2015/10/24.
 */
public class AnbayashiData {
    private int number;
    private int addition;
    private String comment;

    public AnbayashiData(int number, int addition, String comment) {
        this.number = number;
        this.addition = addition;
        this.comment = comment;
    }

    public int getNumber() {
        return number;
    }

    public int getAddition() {
        return addition;
    }

    public String getComment() {
        return comment;
    }
}
